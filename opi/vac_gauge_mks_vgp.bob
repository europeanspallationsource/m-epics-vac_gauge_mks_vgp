<?xml version="1.0" encoding="UTF-8"?>
<display version="2.0.0">
  <name>$(DEVICENAME)</name>
  <width>751</width>
  <widget type="rectangle" version="2.0.0">
    <name>Gauge Background</name>
    <width>751</width>
    <height>600</height>
    <line_color>
      <color name="GROUP-BORDER" red="150" green="155" blue="151">
      </color>
    </line_color>
    <background_color>
      <color name="GROUP-BACKGROUND" red="200" green="205" blue="201">
      </color>
    </background_color>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Gauge</name>
    <text>$(DEVICENAME)</text>
    <width>751</width>
    <height>35</height>
    <font>
      <font name="Header 2" family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
      </font>
    </font>
    <foreground_color>
      <color name="WHITE" red="255" green="255" blue="255">
      </color>
    </foreground_color>
    <background_color>
      <color name="GROUP-BORDER" red="150" green="155" blue="151">
      </color>
    </background_color>
    <transparent>false</transparent>
    <horizontal_alignment>1</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
  </widget>
  <widget type="group" version="2.0.0">
    <name>Group Relays</name>
    <x>10</x>
    <y>55</y>
    <width>526</width>
    <height>252</height>
    <style>3</style>
    <widget type="rectangle" version="2.0.0">
      <name>Relay Background</name>
      <width>526</width>
      <height>252</height>
      <line_color>
        <color name="BLUE-GROUP-BORDER" red="138" green="167" blue="167">
        </color>
      </line_color>
      <background_color>
        <color name="BLUE-GROUP-BACKGROUND" red="179" green="209" blue="209">
        </color>
      </background_color>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Relays Header</name>
      <text>RELAYS</text>
      <width>526</width>
      <height>30</height>
      <font>
        <font name="Header 2" family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
        </font>
      </font>
      <foreground_color>
        <color name="GRAY-TEXT" red="255" green="255" blue="255">
        </color>
      </foreground_color>
      <background_color>
        <color name="BLUE-GROUP-BORDER" red="138" green="167" blue="167">
        </color>
      </background_color>
      <transparent>false</transparent>
      <horizontal_alignment>1</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="embedded" version="2.0.0">
      <name>MKS Gauge Relay Labels</name>
      <file>vac_gauge_mks_relay_labels.bob</file>
      <y>40</y>
      <width>80</width>
      <height>200</height>
      <resize>1</resize>
      <transparent>true</transparent>
    </widget>
    <widget type="embedded" version="2.0.0">
      <name>MKS Gauge Relay Control 1</name>
      <file>vac_gauge_mks_relay_control.bob</file>
      <macros>
        <RELAY>1</RELAY>
      </macros>
      <x>90</x>
      <y>40</y>
      <width>208</width>
      <height>202</height>
      <resize>2</resize>
      <transparent>true</transparent>
    </widget>
    <widget type="embedded" version="2.0.0">
      <name>MKS Gauge Relay Control 2</name>
      <file>vac_gauge_mks_relay_control.bob</file>
      <macros>
        <RELAY>2</RELAY>
      </macros>
      <x>308</x>
      <y>40</y>
      <width>208</width>
      <height>202</height>
      <resize>2</resize>
      <transparent>true</transparent>
    </widget>
  </widget>
  <widget type="group" version="2.0.0">
    <name>Group Pressure</name>
    <x>10</x>
    <y>327</y>
    <width>370</width>
    <height>115</height>
    <style>3</style>
    <widget type="rectangle" version="2.0.0">
      <name>Pressure Background</name>
      <width>370</width>
      <height>115</height>
      <line_color>
        <color name="BLUE-GROUP-BORDER" red="138" green="167" blue="167">
        </color>
      </line_color>
      <background_color>
        <color name="BLUE-GROUP-BACKGROUND" red="179" green="209" blue="209">
        </color>
      </background_color>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Pressure Header</name>
      <text>PRESSURE</text>
      <width>370</width>
      <height>30</height>
      <font>
        <font name="Header 2" family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
        </font>
      </font>
      <foreground_color>
        <color name="GRAY-TEXT" red="255" green="255" blue="255">
        </color>
      </foreground_color>
      <background_color>
        <color name="BLUE-GROUP-BORDER" red="138" green="167" blue="167">
        </color>
      </background_color>
      <transparent>false</transparent>
      <horizontal_alignment>1</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Pressure</name>
      <text>Pressure:</text>
      <y>40</y>
      <width>120</width>
      <height>35</height>
      <horizontal_alignment>2</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="textupdate" version="2.0.0">
      <name>PressureR</name>
      <pv_name>$(DEVICENAME):PrsR</pv_name>
      <x>130</x>
      <y>40</y>
      <width>110</width>
      <height>35</height>
      <format>2</format>
      <horizontal_alignment>2</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="textupdate" version="2.0.0">
      <name>PressureStatR</name>
      <pv_name>$(DEVICENAME):PrsStatR</pv_name>
      <x>250</x>
      <y>40</y>
      <width>110</width>
      <height>35</height>
      <horizontal_alignment>1</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="label" version="2.0.0">
      <name>ATM Calibration</name>
      <text>ATM Calibration:</text>
      <y>85</y>
      <width>120</width>
      <height>25</height>
      <horizontal_alignment>2</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="textentry" version="3.0.0">
      <name>ATM CalibS</name>
      <pv_name>$(DEVICENAME):ATMCalibPrsS</pv_name>
      <x>130</x>
      <y>85</y>
      <width>110</width>
      <height>25</height>
      <format>2</format>
    </widget>
    <widget type="textupdate" version="2.0.0">
      <name>ATM CalibR</name>
      <pv_name>$(DEVICENAME):ATMCalibPrsR</pv_name>
      <x>250</x>
      <y>85</y>
      <width>110</width>
      <height>25</height>
      <format>2</format>
      <horizontal_alignment>2</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
    </widget>
  </widget>
  <widget type="group" version="2.0.0">
    <name>Group Misc</name>
    <x>400</x>
    <y>327</y>
    <width>341</width>
    <height>145</height>
    <style>3</style>
    <widget type="label" version="2.0.0">
      <name>Autozero</name>
      <text>Autozero Channel:</text>
      <y>35</y>
      <width>125</width>
      <height>25</height>
      <horizontal_alignment>2</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="combo" version="2.0.0">
      <name>AutozeroS</name>
      <pv_name>$(DEVICENAME):AutozeroChanS</pv_name>
      <x>135</x>
      <y>35</y>
      <height>25</height>
    </widget>
    <widget type="textupdate" version="2.0.0">
      <name>AutozeroR</name>
      <pv_name>$(DEVICENAME):AutozeroChanR</pv_name>
      <x>241</x>
      <y>35</y>
      <height>25</height>
      <horizontal_alignment>1</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Gas Type</name>
      <text>Gas Type:</text>
      <y>70</y>
      <width>125</width>
      <height>25</height>
      <horizontal_alignment>2</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="combo" version="2.0.0">
      <name>GasTypeS</name>
      <pv_name>$(DEVICENAME):GasTypeS</pv_name>
      <x>135</x>
      <y>70</y>
      <height>25</height>
    </widget>
    <widget type="textupdate" version="2.0.0">
      <name>GasTypeR</name>
      <pv_name>$(DEVICENAME):GasTypeR</pv_name>
      <x>241</x>
      <y>70</y>
      <height>25</height>
      <horizontal_alignment>1</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Power Status</name>
      <text>Power:</text>
      <width>125</width>
      <height>25</height>
      <horizontal_alignment>2</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="combo" version="2.0.0">
      <name>PowerStatusS</name>
      <pv_name>$(DEVICENAME):PowerS</pv_name>
      <x>135</x>
      <height>25</height>
    </widget>
    <widget type="led" version="2.0.0">
      <name>PowerStatusR</name>
      <pv_name>$(DEVICENAME):PowerR</pv_name>
      <x>241</x>
      <width>100</width>
      <height>25</height>
      <square>true</square>
      <labels_from_pv>true</labels_from_pv>
      <tooltip>$(pv_name)$(pv_value)</tooltip>
    </widget>
    <widget type="action_button" version="3.0.0">
      <name>Zero Gague</name>
      <actions>
        <action type="write_pv">
          <pv_name>$(pv_name)</pv_name>
          <value>1</value>
          <description>Zero the Gauge</description>
        </action>
      </actions>
      <pv_name>$(DEVICENAME):ZeroS</pv_name>
      <x>191</x>
      <y>115</y>
      <width>150</width>
      <tooltip>$(actions)</tooltip>
    </widget>
  </widget>
  <widget type="rectangle" version="2.0.0">
    <name>Separator</name>
    <x>5</x>
    <y>499</y>
    <width>741</width>
    <height>3</height>
    <line_color>
      <color name="GROUP-BORDER" red="150" green="155" blue="151">
      </color>
    </line_color>
    <background_color>
      <color name="GROUP-BORDER" red="150" green="155" blue="151">
      </color>
    </background_color>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>Controller Detailed</name>
    <actions>
      <action type="open_display">
        <file>vac_ctrl_mks946_937b_controller.bob</file>
        <macros>
          <DEVICENAME>$(CONTROLLERNAME)</DEVICENAME>
        </macros>
        <target>standalone</target>
        <description>Open Controller Screen</description>
      </action>
    </actions>
    <x>570</x>
    <y>524</y>
    <width>150</width>
    <height>50</height>
    <tooltip>$(actions)</tooltip>
  </widget>
</display>
