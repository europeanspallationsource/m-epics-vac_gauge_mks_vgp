# Vacuum Pirani Gauge MKS

**DEPRECATED**
**Please use vac_ctrl_mks946_937b with the built-in vac_gauge_mks_vgp snippet**

EPICS module to read/write data for MKS vacuum pirani gauge.

IOC does not directly communicate with the gauge, the gauge is actully connected to a vacuum controller. IOC communicated to the controller and controller provides the gauge data. Controllers can have multiple gauges in deifferent channels.


The modules arrangement should be something like:
```
IOC-|
    |-vac-ctrl-mks946_937b
	|	   |-vac-gauge-mks-vgp (Pirani gauge)
	|	   |-vac-gauge-mks-vgp (Pirani gauge)
	|      |-vac-gauge-mks-vgc (Cold cathode gauge)
	|
	|-vac-ctrl-mks946_937b
	|	   |-vac-gauge-mks-vgp (Pirani gauge)
	|	   |-vac-mfc-mks-gv50a (Mass flow controller)
```

This allows EPICS modules for gauges to be re-used and any combination of controller and gauges to be built from existing modules.

**This version of the module requires at least v3.0.4 of vac_ctrl_mks946_937b**

## Startup Examples

`iocsh -r vac_ctrl_mks946_937b,catania -c 'requireSnippet(vac_ctrl_mks946_937b_ethernet.cmd,"DEVICENAME=LNS-LEBT-010:VAC-VEVMC-01100, IPADDR=10.4.0.213, PORT=4004")' -r vac_gauge_mks_vgp,catania -c 'requireSnippet(vac_gauge_mks_vgp.cmd, "DEVICENAME=LNS-LEBT-010:VAC-VGP-10000, CONTROLLERNAME=LNS-LEBT-010:VAC-VEVMC-01100, CHANNEL=1, CHANNEL_C=3, RELAY1=1, RELAY2=2")'`

If ran as proper ioc service:
```
epicsEnvSet(DEVICENAME, "LNS-LEBT-010:VAC-VGP-00031")
epicsEnvSet(CONTROLLERNAME, "LNS-LEBT-010:VAC-VEG-10010")
epicsEnvSet(CHANNEL, "6")
epicsEnvSet(CHANNEL_C, "6")
epicsEnvSet(RELAY1, "11")
epicsEnvSet(RELAY2, "12")
require vac_gauge_mks_vgp, 2.0.0-catania
< ${REQUIRE_vac_gauge_mks_vgp_PATH}/startup/vac_gauge_mks_vgp.cmd
```
